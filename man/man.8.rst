..
   Copyright (C) 2025 Internet Systems Consortium, Inc. ("ISC")

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   See the COPYRIGHT file distributed with this work for additional
   information regarding copyright ownership.


``stork-server-ldap`` - The Stork Server hook for LDAP authentication
--------------------------------------------------------------------------

Description
~~~~~~~~~~~

The ``stork-server-ldap`` is a hook/plugin for the ``stork-server``
application that enables an authentication mechanism based on LDAP (Lightweight
Directory Access Protocol). Hook is a binary file with the .so extension, and
must be placed in the server hook directory

Arguments
~~~~~~~~~

The hook behavior can be controlled with command-line switches and/or
environment variables that must to be passed to ``stork-server``. The
environment variables can be set before running the server (equivalent
environment variables are listed in square brackets, where applicable).

``--ldap.url=``
   The LDAP server access URL (use ldaps:// protocol to connect over TLS) (default: ldap://127.0.0.1:1389). ``[$STORK_SERVER_HOOK_LDAP_URL]``
   
``--ldap.root=``
   The LDAP root for login user (default: dc=example,dc=org). ``[$STORK_SERVER_HOOK_LDAP_ROOT]``
   
``--ldap.bind-userdn=``
   The maintenance userdn used to bind to the server for reading user profiles (default: cn=admin,dc=example,dc=org). ``[$STORK_SERVER_HOOK_LDAP_BIND_USERDN]``
   
``--ldap.bind-password=``
   The maintenance password used to bind to the server for reading user profiles (default: adminpassword). ``[$STORK_SERVER_HOOK_LDAP_BIND_PASSWORD]``
   
``--ldap.skip-tls-server-verification``
   Skip the TLS server certificate verification - not recommended for the production environments. ``[$STORK_SERVER_HOOK_LDAP_SKIP_SERVER_TLS_VERIFICATION]``
   
``--ldap.group-allow=``
   The mandatory group that must be assigned to user to access Stork, empty for allow all users. ``[$STORK_SERVER_HOOK_LDAP_GROUP_ALLOW]``
   
``--ldap.map-groups``
   Enable mapping LDAP groups into Stork groups. ``[$STORK_SERVER_HOOK_LDAP_MAP_GROUPS]``
   
``--ldap.debug``
   Enable additional debug information about connection to LDAP server. ``[$STORK_SERVER_HOOK_LDAP_DEBUG]``
   
``--ldap.timeout=``
   The LDAP server connection timeout (default: 30s). ``[$STORK_SERVER_HOOK_LDAP_TIMEOUT]``

LDAP to Stork group mapping:

``--ldap.group-admin=``
   The LDAP group corresponding to Stork 'admin' group (default: stork-admin). ``[$STORK_SERVER_HOOK_LDAP_GROUP_ADMIN]``

``--ldap.group-super-admin=``
   The LDAP group corresponding to Stork 'super-admin' group (default: stork-super-admin). ``[$STORK_SERVER_HOOK_LDAP_GROUP_SUPER_ADMIN]``

LDAP schema attributes:

``--ldap.object-class-group=``
   The name of the group object class in the group schema (preferred 'groupOfNames' or 'groupOfUniqueNames') (default: groupOfNames). ``[$STORK_SERVER_HOOK_LDAP_OBJECT_CLASS_GROUP]``

``--ldap.object-class-group-member=``
   The name of the member property in the group object class (for 'groupOfUniqueNames' use 'uniqueMember') (default: member). ``[$STORK_SERVER_HOOK_LDAP_OBJECT_CLASS_GROUP_MEMBER]``

``--ldap.object-class-group-common-name=``
   The name of the common name property in the group object class (default: cn). ``[$STORK_SERVER_HOOK_LDAP_OBJECT_CLASS_GROUP_COMMON_NAME]``

``--ldap.object-class-user=``
   The name of the user object class in the user schema (default: organizationalPerson). ``[$STORK_SERVER_HOOK_LDAP_OBJECT_CLASS_USER]``

``--ldap.object-class-user-id=``
   The name of the ID property in the user object class (default: uid). ``[$STORK_SERVER_HOOK_LDAP_OBJECT_CLASS_USER_ID]``

``--ldap.object-class-user-first-name=``
   The name of the first name property in the user object class, optional (default: givenName). ``[$STORK_SERVER_HOOK_LDAP_OBJECT_CLASS_USER_FIRST_NAME]``

``--ldap.object-class-user-last-name=``
   The name of the last name property in the user object class, optional (default: sn). ``[$STORK_SERVER_HOOK_LDAP_OBJECT_CLASS_USER_LAST_NAME]``

``--ldap.object-class-user-email=``
   The name of the email property in the user object class, optional (default: mail). ``[$STORK_SERVER_HOOK_LDAP_OBJECT_CLASS_USER_EMAIL]``

Stork evaluates and prioritizes the settings it receives based on where they are applied.
Command-line flags have the highest priority; next are parameters from the
environment file. The lowest priority is given to environment variables.

Mailing Lists and Support
~~~~~~~~~~~~~~~~~~~~~~~~~

There are public mailing lists available for the Stork project. **stork-users**
(stork-users at lists.isc.org) is intended for Stork users. **stork-dev**
(stork-dev at lists.isc.org) is intended for Stork developers, prospective
contributors, and other advanced users. The lists are available at
https://www.isc.org/mailinglists. The community provides best-effort support
on both of those lists.

See Also
~~~~~~~~

:manpage:`stork-server(8)`
