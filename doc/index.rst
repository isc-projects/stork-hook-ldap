#########
LDAP Hook
#########

The Stork server can use an external LDAP server to identify and authorize users and groups. It is
typically used to provide single-sign-on (SSO) via an organization's personnel directory. If you are not familiar with
LDAP, a `brief introduction <https://kb.isc.org/docs/ldap-intro>`_ is available.

Installation
============

The recommended way to install the LDAP hook, and Stork itself, is to use the pre-compiled packages provided by ISC.
Please see the Stork ARM for instructions on obtaining and installing ISC packages.

If building from source, you must clone the repository or download the source tarball from
`the LDAP hook GitLab project <https://gitlab.isc.org/isc-projects/stork-hook-ldap>`_. See the Stork ARM for more about
building from source.

Introduction
============

LDAP support in Stork is provided by the stork-hook-ldap hook (plugin/library). Please copy ``stork-hook-ldap.so`` to
the server hook directory (``/usr/lib/stork-server/hooks`` by default). The Stork server automatically loads any hooks
it finds there.

The LDAP hook has a number of configuration options controlling its behavior. They are specified by command-line options or environment variables when the stork-server process is started. Typically, the environment variables are specified in the
``/etc/stork/server.env``. Consult the Stork ARM for more information about
configuring Stork.

Once you have configured Stork to use LDAP hook, the new "LDAP" authentication option will appear on the Stork login page. Use the "Pick a Method" drop down list to select "LDAP". Enter user name and password.
Stork will use the hook to contact the LDAP server, validate the password, and
log in the user to Stork.

Configuration
=============

The most commonly used options, listed below, are typically defined in ``/etc/stork/server.env``. If you are unsure what
values to use, consult your LDAP directory administrator.

Option Summary
--------------

.. list-table:: LDAP Hook Options
    :header-rows: 1

    * - Option
      - Description
    * - STORK_SERVER_HOOK_LDAP_URL
      - LDAP server to connect to
    * - STORK_SERVER_HOOK_BIND_USERDN
      - Stork LDAP bind identity
    * - STORK_SERVER_HOOK_BIND_PASSWORD
      - Password for USERDN
    * - STORK_SERVER_HOOK_LDAP_ROOT
      - Base DN context for Stork users
    * - STORK_SERVER_HOOK_LDAP_GROUP_ALLOW
      - Limit Stork users to an LDAP context
    * - STORK_SERVER_HOOK_MAP_GROUPS
      - Enable mapping to Stork roles
    * - STORK_SERVER_HOOK_LDAP_GROUP_SUPER_ADMIN
      - CN of the group to map to Stork super-admin
    * - STORK_SERVER_HOOK_LDAP_GROUP_ADMIN
      - CN of the group to map to Stork admin

Connection to LDAP Server
-------------------------

You must configure a connection from Stork to the LDAP server. This is used by Stork to connect to the directory,
make queries, and verify passwords.

- ``STORK_SERVER_HOOK_LDAP_URL``

    Specify the URI to your LDAP server. It must include protocol and port. For example,
    ``ldap://nyc-dc1.example.com:389`` or ``ldaps://198.51.100.42:636``.

- ``STORK_SERVER_HOOK_BIND_USERDN``

    Provide the user identity Stork will use to login to the LDAP server (also called binding). You must provide a fully
    qualified Distinguished Name (DN). This is not necessarily a regular user, nor a user of Stork. The DN context of
    this user may differ from ``STORK_SERVER_HOOK_LDAP_ROOT``.

- ``STORK_SERVER_HOOK_BIND_PASSWORD``

    Indicate the password for the user given in ``STORK_SERVER_HOOK_BIND_USERDN``. If the password contains characters
    special to the Unix shell, they may need to be quoted or escaped.

User Mapping
------------

You must tell Stork how to find your Stork users in the directory.

- ``STORK_SERVER_HOOK_LDAP_ROOT``

    Specify an LDAP naming context used as the base DN for all users. You must provide a fully qualified Distinguished
    Name (DN). For example, if Stork users have a DN like ``CN=John Smith,OU=IT Staff,DC=example,DC=com``, then the root
    should be ``OU=IT Staff,DC=example,DC=com``.

- ``STORK_SERVER_HOOK_LDAP_GROUP_ALLOW``

    If defined, Stork users will be limited to members of the specified LDAP group. You must provide the Common Name of
    the group (without ``CN=`` prefix). Use this if not all LDAP users should be permitted access to Stork. For example,
    if you have an LDAP group ``dhcpadmins``, you might specify that here, and put only Stork users in that group.

Group Mappings
--------------

Optionally, you can configure mappings between LDAP groups and the Stork roles. This allows Stork to automatically
follow permission changes in your organization's directory.

- ``STORK_SERVER_HOOK_MAP_GROUPS``

    First, this must be set to true to enable the group mapping feature.

- ``STORK_SERVER_HOOK_LDAP_GROUP_SUPER_ADMIN``

    Members of this LDAP group will be granted the super-admin role in Stork. Specify the Common Name of an LDAP group
    (without CN= prefix).

- ``STORK_SERVER_HOOK_LDAP_GROUP_ADMIN``

    Members of this LDAP group will be granted the admin role in Stork. Specify the Common Name of an LDAP group
    (without ``CN=`` prefix).

See Also
========

- `stork-hook-ldap <https://gitlab.isc.org/isc-projects/stork-hook-ldap/>`_ on the ISC GitLab
