* 3 [func] ! robin.berger

    Separated the bind user domain name (DN) from the root DN used to
    log in users.
    (Gitlab #1325)

* 2 [build] andrei

    Add coverage reporting to unit tests.
    (Gitlab #1174)

* 1 [func] slawek

    Implemented the initial version of the LDAP hook for Stork server.
    (Gitlab #638)


LEGEND

[bug]   General bug fix.  This is generally a backward compatible change,
unless it's deemed to be impossible or very hard to keep
compatibility to fix the bug.
[build] Compilation and installation infrastructure change.
[doc]   Update to documentation. This shouldn't change run time behavior.
[func]  New feature.  In some cases this may be a backward incompatible
change, which would require a bump of major version.
[sec]   Security hole fix. This is no different than a general bug
fix except that it will be handled as confidential and will cause
security patch releases.
[perf]  Performance related change.
[ui]    User Interface change.

Header syntax:
[Leading asterisk] [Entry number] [Category] [Incompatibility mark (optional)] [Author]
The header components are delimited by a single space.
The backward incompatible or operational change is indicating by the
exclamation mark (!).
