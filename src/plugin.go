package main

import (
	"isc.org/stork-hook-ldap/internal"
	"isc.org/stork/hooks"
)

// Loads a callout carrier (an object with the callout specification implementations).
func Load(rawSettings hooks.HookSettings) (hooks.CalloutCarrier, error) {
	settings, ok := rawSettings.(*internal.Settings)
	if !ok {
		return nil, errInvalidSettings
	}

	return &calloutCarrier{
		// Make a copy to avoid side-effects in core.
		settings: *settings,
	}, nil
}

// Returns an application name and expected version.
func GetVersion() (string, string) {
	return hooks.HookProgramServer, hooks.StorkVersion
}

// Returns the CLI flags object. The CLI flags are defined by its fields' tags.
func CreateCLIFlags() hooks.HookSettings {
	return &internal.Settings{}
}

// Type guards.
var (
	_ hooks.HookLoadFunction           = Load
	_ hooks.HookGetVersionFunction     = GetVersion
	_ hooks.HookCreateCLIFlagsFunction = CreateCLIFlags
)
