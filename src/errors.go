package main

import "errors"

// Errors.
var (
	errInvalidSettings = errors.New("LDAP hook invalid settings type")
	errValidation      = errors.New("LDAP hook validation error")
	errUnauthorized    = errors.New("LDAP hook unauthorized error")
)
