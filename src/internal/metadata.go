package internal

import (
	"embed"
	"io"

	"isc.org/stork/hooks/server/authenticationcallouts"
)

//go:embed icon.png
var icon embed.FS

// Authentication method Metadata.
type Metadata struct{}

// GetIdentifierFormLabel implements authenticationcallouts.AuthenticationMetadataForm.
func (*Metadata) GetIdentifierFormLabel() string {
	return "Username"
}

// GetSecretFormLabel implements authenticationcallouts.AuthenticationMetadataForm.
func (*Metadata) GetSecretFormLabel() string {
	return "Password"
}

// GetDescription implements authenticationcallouts.AuthenticationMetadata.
func (*Metadata) GetDescription() string {
	return "Lightweight Directory Access Protocol service provides a central place to store usernames and passwords."
}

// GetID implements authenticationcallouts.AuthenticationMetadata.
func (*Metadata) GetID() string {
	return "ldap"
}

// GetIcon implements authenticationcallouts.AuthenticationMetadata.
func (*Metadata) GetIcon() (io.ReadCloser, error) {
	return icon.Open("icon.png")
}

// GetName implements authenticationcallouts.AuthenticationMetadata.
func (*Metadata) GetName() string {
	return "LDAP"
}

var (
	_ authenticationcallouts.AuthenticationMetadata     = (*Metadata)(nil)
	_ authenticationcallouts.AuthenticationMetadataForm = (*Metadata)(nil)
)
