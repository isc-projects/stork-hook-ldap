package main

import (
	"testing"

	"github.com/stretchr/testify/require"
	"isc.org/stork-hook-ldap/internal"
	"isc.org/stork/hooks"
)

// Tests that the plugin can be loaded.
func TestLoad(t *testing.T) {
	// Arrange
	settings := &internal.Settings{}

	// Act
	carrier, err := Load(settings)

	// Assert
	require.NoError(t, err)
	require.NotNil(t, carrier)
}

// Tests that the settings are mandatory.
func TestLoadNoSettings(t *testing.T) {
	// Act
	carrier, err := Load(nil)

	// Assert
	require.Error(t, err)
	require.Nil(t, carrier)
}

// Tests that the plugin version is returned.
func TestGetVersion(t *testing.T) {
	// Act
	name, version := GetVersion()

	// Assert
	require.Equal(t, hooks.HookProgramServer, name)
	require.Equal(t, hooks.StorkVersion, version)
}

// Tests that the CLI flags are returned.
func TestCreateCLIFlags(t *testing.T) {
	// Act
	flags := CreateCLIFlags()

	// Assert
	require.NotNil(t, flags)
	require.IsType(t, &internal.Settings{}, flags)
}
